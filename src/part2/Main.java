package sixthLessonHomeTask.Part2;

public class Main {

    public static void main(String[] args){
        System.out.println("\nWelcome to Volvo Dealership! Thank you for choosing us. Volvo ® - For life.");
        welcome();
    }

    public static void welcome(){
        System.out.println("\nYou can choose interesting information for you." +
                "\n1 - New Arrivals" +
                "\n2 - Dealer's Clients" +
                "\n3 - Book a car" +
                "\n4 - Bought cars" +
                "\n5 - Exit");
        System.out.print("Please enter what you need: ");

        String key = Dealership.inInformation();

        switch (key){
            case "1": Dealership.doOne(); break;
            case "2": Dealership.doTwo(); break;
            case "3": Dealership.doThree(); break;
            case "4": Dealership.doFour(); break;
            case "5": Dealership.doExit(); break;
            default: {
                System.out.println("Error: Input data is not correct.");
            }
        }
    }

}
