package sixthLessonHomeTask.Part2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Dealership {

    protected static ArrayList<String> carBrand = new ArrayList<>();
    protected static ArrayList<Double> carIngineVolume = new ArrayList<>();
    protected static ArrayList<Double> carAccelerationTo100 = new ArrayList<>();
    protected static ArrayList<String> carBodyType = new ArrayList<>();
    protected static ArrayList<Integer> carRealiseYear = new ArrayList<>();

    protected static ArrayList<String> clientName = new ArrayList<>();
    protected static ArrayList<String> clientCar = new ArrayList<>();
    protected static ArrayList<String> carStatus = new ArrayList<>();
    protected static ArrayList<String> carDateBought = new ArrayList<>();

    public static Scanner scanner = new Scanner(System.in);

    public static String blueText = "\u001B[36m", redText = "\u001B[31m", whiteText = "\u001B[30m";
    public static String blackText = "\u001B[97m", resetText = "\u001B[0m";
    public static String whiteBackground = "\u001B[40m", blackBackground = "\u001B[107m";

    public static String inInformation(){
        return scanner.nextLine();
    }

    static {
        carBrand.add("Volvo XC90");
        carBrand.add("Volvo V60");
        carBrand.add("Volvo S90");
        carBrand.add("Volvo C70");

        carIngineVolume.add(2.0);
        carIngineVolume.add(2.0);
        carIngineVolume.add(2.0);
        carIngineVolume.add(2.4);

        carAccelerationTo100.add(7.8);
        carAccelerationTo100.add(7.9);
        carAccelerationTo100.add(6.8);
        carAccelerationTo100.add(9.0);

        carBodyType.add("off-road");
        carBodyType.add("station wagon");
        carBodyType.add("sedan");
        carBodyType.add("cabriolet");

        carRealiseYear.add(2017);
        carRealiseYear.add(2018);
        carRealiseYear.add(2016);
        carRealiseYear.add(2017);

        clientName.add("Will Smith");
        clientName.add("Johny Depp");
        clientName.add("Tom Henks");
        clientName.add("Emma Watson");
        clientName.add("Penelopa Cruz");

        clientCar.add(carBrand.get(0));
        clientCar.add(carBrand.get(3));
        clientCar.add(carBrand.get(1));
        clientCar.add(carBrand.get(2));
        clientCar.add(carBrand.get(3));

        carDateBought.add("25/02/2017");
        carDateBought.add("01/06/2017");
        carDateBought.add("16/03/2018");
        carDateBought.add("30/04/2016");
        carDateBought.add("09/10/2017");

        carStatus.add("Bought");
        carStatus.add("Bought");
        carStatus.add("Bought");
        carStatus.add("Bought");
        carStatus.add("Bought");
    }

    public static void showInformation(){
        System.out.print(whiteBackground + blackText);
        System.out.println("Brand\t\t\tVolume\tAccel.\tYear\tBody");
        for (int i = 0; i < carBrand.size(); i++){
            if (i % 2 == 0){
                System.out.print(whiteText);
            }
            else{
                System.out.print(whiteBackground + blackText);
            }
            System.out.println(carBrand.get(i) + "\t\t" + carIngineVolume.get(i) + "\t\t"
                    + carAccelerationTo100.get(i) + "\t\t" + carRealiseYear.get(i) + "\t"
                    + carBodyType.get(i));
        }
    }

    public static void doOne(){
        showInformation();
        doContinue();
    }

    public static void doTwo(){
        System.out.print(whiteBackground + blackText);
        System.out.println("Brand\t\t\tName");
        doPrint();
        doContinue();
    }

    private static void doPrint() {
        for (int i = 0; i < clientCar.size(); i++){
            if (i % 2 == 0){
                System.out.print(whiteText);
            }
            else {
                System.out.print(whiteBackground + blackText);
            }
            System.out.println(clientCar.get(i) + "\t\t" + clientName.get(i));
        }
    }

    public static void doThree(){
        System.out.print("\nEnter your name: ");
        String name = inInformation();
        clientName.add(name);
        System.out.println("Hello " + name + "! You can book next car:");

        showInformation();

        System.out.print(resetText + "Enter a model car which you like: ");
        String car = inInformation();
        clientCar.add(car);
        carDateBought.add(getDate());
        carStatus.add("Booked");

        System.out.println("Car \"" + car + "\" booked in name \"" + name + "\"");
        System.out.print(whiteBackground + blackText);
        System.out.println("Brand\t\t\tName");
        doPrint();
        doContinue();
    }

    public static void doFour(){
        System.out.print(whiteBackground + blackText);
        System.out.println("Date\t\t\tStatus\t\tBrand\t\t\tName");
        for (int i = 0; i < clientCar.size(); i++){
            if (i % 2 == 0){
                System.out.print(redText);
            }
            else {
                System.out.print(whiteBackground + redText);
            }
            System.out.println(carDateBought.get(i) + "\t\t" + carStatus.get(i) + "\t\t" + clientCar.get(i)
                    + "\t\t" + clientName.get(i));
        }
        doContinue();
    }

    public static void doContinue(){
        System.out.println(resetText);

        System.out.println("Press any key to continue or 0 to exit");
        switch (inInformation()){
            case "0" : System.exit(0);
            default: Main.welcome();
        }
    }

    public static String getDate(){
        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date date = new Date();
        return dateFormat.format(date);
    }

    public static void doExit(){
        System.exit(0);
    }

    public static void showColor(){
        for (int i = 0; i <= 200; i++){
            System.out.println("\u001B[" + i + "m" + "Hello" + i);
        }
    }
}
